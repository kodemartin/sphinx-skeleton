# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line.
SPHINXOPTS    = 
SPHINXBUILD   = sphinx-build
SOURCEDIR     = source
BUILDDIR      = build
# Configure the generation of apidocs
SPHINXAPIDOC  = sphinx-apidoc
SPHINXAPIOPTS    = 
MODPATH =


# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

# Rule to construct API docs
apidoc: 
	@$(SPHINXAPIDOC) $(SPHINXAPIOPTS) -eTM -o \
	    "$(SOURCEDIR)/_modules" $(MODPATH)

clean-repo:
	rm -rf .git
	find ./ -maxdepth 2 -type f -name '.*' -and ! -name '.gitlab*' \
	    -exec rm -f {} +
	rm LICENSE requirements.txt

.PHONY: help Makefile apidoc clean-repo test-clean-repo

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
