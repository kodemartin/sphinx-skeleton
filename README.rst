sphinx-skeleton
===============

A template configuration for generating sphinx docs
---------------------------------------------------

Features
^^^^^^^^

* Separate ``source`` and ``build`` directories.
* Expanded ``Makefile`` that generates automatically API docs
  based on local packages.

Checkout the `live demo <https://kodemartin.gitlab.io/sphinx-skeleton>`_ of this
template.

Using the template
------------------

#. Copy the template into your project::

      $ git clone <this-repo> path/to/docs/
      $ make -C path/to/docs/ clean-repo

#. Set configuration options in ``path/to/docs/source/conf.py``.

#. Add content in ``path/to/docs/source/chapters/``.

#. Modify ``path/to/docs/source/index.rst`` accordingly.

#. Generate documentation

   * Continuous integration (applies only to ``gitlab``)::

         $ mv path/to/docs/.gitlab-ci.yml ./

     Alternatively, you can explicitly
     define the path to ``.gitlab-ci.yml`` in your project settings.

     .. important::

         Don't forget to override ``DOCSPATH`` and ``MODPATH`` in ``.gitlab-ci.yml``:

         * ``DOCSPATH`` should be assigned the value of the path you cloned the repository
           into.
         * ``MODPATH`` should be assinged the value of the path of the python-package
           to be documented.

     By default CI is configured to run only when a tag is created.

   * Manual generation

     #. Create API docs for your package::

           $ make MODPATH=<package-path> -C path/to/docs/ apidoc

        where ``<package-path>`` is the path of the package **relative**
        to ``path/to/docs``.

     #. Build documentation::

           $ make -C path/to/docs/ html

License
-------

This template is published under the `MIT LICENSE <LICENSE>`_.

Credits
-------

* The default theme is `Alabaster <https://github.com/bitprophet/alabaster>`_.
* The default logo is a free image from https://pixabay.com.
* The skeleton is based on an automatically generated structure of files
  through ``sphinx-quickstart``. See `Sphinx docs <http://www.sphinx-doc.org>`_
  for more details.
